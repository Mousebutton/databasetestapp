package nl.ikbenali.Databasetestapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.*;

public class Databasetestapp extends Application {

    public static void main(String args[]) throws SQLException {
        launch(args);
    }

    public void start(final Stage primaryStage) throws Exception {
        final Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("main.fxml"));
        final Scene scene = new Scene(root,300,300);

        primaryStage.setTitle("Database test app");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
