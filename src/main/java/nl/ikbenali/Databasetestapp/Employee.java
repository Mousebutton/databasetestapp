package nl.ikbenali.Databasetestapp;

import javax.swing.*;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Employee {
    private SimpleStringProperty id;
    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty Phone;
    private StringProperty email;

    public Employee()
    {
        this(0, "", "", "", "");
    }

    public Employee(int id, String firstName,String lastName, String Phone, String email) {
        this.id = new SimpleStringProperty(Integer.toString(id));
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.Phone = new SimpleStringProperty(Phone);
        this.email = new SimpleStringProperty(email);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }
    public StringProperty idProperty() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public int getId() {
        return Integer.parseInt(id.getValue().toString());
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getPhone() {
        return Phone.get();
    }

    public StringProperty phoneProperty() {
        return Phone;
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }
}
