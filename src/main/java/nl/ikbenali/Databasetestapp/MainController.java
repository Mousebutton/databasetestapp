package nl.ikbenali.Databasetestapp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Pair;


public class MainController {
    @FXML
    private TableView tblOrders;
    @FXML
    private Button btnClick;
    @FXML
    private Button btnAddProduct;
    @FXML
    private ChoiceBox drpRetailer;
    @FXML
    private ChoiceBox drpOrderMethod;
    @FXML
    private ChoiceBox drpEmployee;
    @FXML
    private ChoiceBox drpProduct;
    @FXML
    private ChoiceBox drpAmount;
    @FXML
    private ListView lstProduct;


    @FXML
    private ChoiceBox drpCustomer;
    @FXML
    private ChoiceBox drpTripDate;
    @FXML
    private ChoiceBox drpPersonsAmount;
    @FXML
    private TextField txtPrice;

    private static final String SELECTIDBYNAMEPRODUCT = "select Product_id from Product where Product_name = ?";
    private static final String SELECTLASTIDORDER_HEADER = "select top (1) Order_nr from Order_Header order by Order_nr desc";
    private static final String SELECTLASTIDBOOKING = "select top (1) Booking_number from Booking order by Booking_number desc";

    private List<Pair<String, Integer>> products;

    private Connection conn;

    @FXML
    public void initialize() throws SQLException {
        conn = DatabaseConnection.getInstance();

        fillDropdownRetailer();
        fillDropdownOrderMethod();
        fillDropdownEmployee();
        fillDropdownProduct();
        fillDropdownAmount();

        fillPersonsAmount();
        fillCustomers();
        fillTripDate();

        products = new ArrayList<>();

        ObservableList items = tblOrders.getItems();

        ((TableColumn) tblOrders.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("id"));
        ((TableColumn) tblOrders.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("firstName"));
        ((TableColumn) tblOrders.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ((TableColumn) tblOrders.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<>("Phone"));
        ((TableColumn) tblOrders.getColumns().get(4)).setCellValueFactory(new PropertyValueFactory<>("email"));

        tblOrders.setRowFactory(tv -> {
            TableRow<Employee> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Employee e = row.getItem();
                    System.out.println(e.getId());
                }
            });
            return row;
        });

        String sql = "SELECT * FROM Employee;";
        try (Statement statement = conn.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                Employee e = new Employee(resultSet.getInt(1), resultSet.getString(4), resultSet.getString(5), resultSet.getString(13), resultSet.getString(16));
                e.setFirstName(resultSet.getString(4));
                items.add(e);
            }
        }
    }

    private void fillTripDate() {
        fillDropdown(drpTripDate,"SELECT Trip_Date_id from Trip_Date");
    }

    private void fillCustomers() {
        fillDropdown(drpCustomer,"SELECT Customer_id from Customer");
    }

    private void fillPersonsAmount() {
        for (int i = 1; i <= 20; i++) {
            drpPersonsAmount.getItems().add(i);
        }
    }

    private void fillDropdown(ChoiceBox c, String sql) {
        try (Statement statement = conn.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {
            ObservableList o = c.getItems();
            while (resultSet.next()) {
                o.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void fillDropdownProduct() {
        fillDropdown(drpProduct, "SELECT Product_name FROM Product");
    }

    private void fillDropdownEmployee() {
        fillDropdown(drpEmployee, "SELECT Employee_id FROM Employee");
    }

    private void fillDropdownOrderMethod() {
        fillDropdown(drpOrderMethod, "SELECT Order_Method_id FROM Order_Method");
    }

    private void fillDropdownRetailer() {
        fillDropdown(drpRetailer, "SELECT Retailer_Site_code FROM Retailer_Site");
    }

    private void fillDropdownAmount() {
        for (int i = 1; i < 20; i++) {
            drpAmount.getItems().add(i);
        }
    }

    private int dbStringToInt(String s, String sql) throws SQLException {
        int result = 0;
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, s);
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int getLastId(String sql) {
        int result = 0;
        try (Statement statement = conn.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @FXML
    private void onBtnClick(ActionEvent event) throws SQLException {
        if (drpAmount.getValue() != null && drpRetailer.getValue() != null && drpProduct.getValue() != null
                && drpEmployee.getValue() != null && drpOrderMethod.getValue() != null && !products.isEmpty()) {
            int retailerId = Integer.parseInt(drpRetailer.getValue().toString());
            int orderMethodId = Integer.parseInt(drpOrderMethod.getValue().toString());
            int employeeId = Integer.parseInt(drpEmployee.getValue().toString());

            String insertQuery = "INSERT INTO Order_Header (Order_nr,Retailer_Site_code,Customer_id,Employee_id,Department_id,Order_Method_id,Order_status_id,order_date)" +
                    "values (?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";
            PreparedStatement statement = null;
            int Order_nr = getLastId(SELECTLASTIDORDER_HEADER) + 1;
            statement = conn.prepareStatement(insertQuery);
            statement.setInt(1, Order_nr);
            statement.setInt(2, retailerId);
            statement.setInt(3, 1); //customer
            statement.setInt(4, employeeId);
            statement.setInt(5, 6); //departmentid
            statement.setInt(6, orderMethodId);
            statement.setInt(7, 1);
            if (statement.execute()) {
                System.out.println("Added order: " + Order_nr);
            }
            for (Pair<String, Integer> product : products) {
                String insertProducts = "INSERT INTO Order_Detail (Order_nr,Product_Id,Quantity,Unit_cost,Unit_price,Unit_sale_price)" +
                        "values (?,?,?,?,?,?)";
                PreparedStatement preparedStatementProducts = conn.prepareStatement(insertProducts);
                preparedStatementProducts.setInt(1, Order_nr);
                preparedStatementProducts.setInt(2, dbStringToInt(product.getKey(), SELECTIDBYNAMEPRODUCT));
                preparedStatementProducts.setInt(3, product.getValue());
                preparedStatementProducts.setFloat(4, 100);
                preparedStatementProducts.setFloat(5, 100);
                preparedStatementProducts.setFloat(6, 100);

                if (preparedStatementProducts.execute()) {
                    System.out.println("Added product to order:" + Order_nr);
                }
            }
        }
    }

    @FXML
    private void onBtnAddProduct(ActionEvent e) throws SQLException {
        String selectedValue = drpProduct.getValue().toString();
        Integer amount = Integer.valueOf(drpAmount.getValue().toString());
        if (lstProduct.getItems().stream().noneMatch(o -> o.equals(selectedValue))) {
            lstProduct.getItems().add(selectedValue);
            products.add(new Pair<>(selectedValue, amount));
            System.out.println(dbStringToInt(selectedValue, SELECTIDBYNAMEPRODUCT));
        }
    }

    @FXML
    private void onBtnBook(ActionEvent e) throws SQLException {
        if (drpCustomer.getValue() != null && drpTripDate.getValue() != null && drpPersonsAmount.getValue() != null && txtPrice.getText() != "") {
            int lastId = getLastId(SELECTLASTIDBOOKING);
            int customerId = Integer.parseInt(drpCustomer.getValue().toString());
            int tripDateId = Integer.parseInt(drpTripDate.getValue().toString());
            int amountOfPersons = Integer.parseInt(drpPersonsAmount.getValue().toString());
            float price = Float.parseFloat(txtPrice.getText());

            String insertQuery = "INSERT INTO Booking (Booking_number,Customer_id,Persons,Trip_Date_id,Total_price) values (?,?,?,?,?)";

            PreparedStatement statement = conn.prepareStatement(insertQuery);
            statement.setInt(1, lastId + 1);
            statement.setInt(2, customerId);
            statement.setInt(3, amountOfPersons);
            statement.setInt(4, tripDateId);
            statement.setFloat(5, price);

            statement.execute();
        }
    }
}
