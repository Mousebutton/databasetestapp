package nl.ikbenali.Databasetestapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Connection conn = null;

    private final static String dbURL = "192.168.56.101:1433";
    private final static String dbLogin = "sa";
    private final static String dbPassword = "aA123456!";

    public static Connection getInstance() throws SQLException {
        if(conn == null)
        {
            conn = DriverManager.getConnection("jdbc:sqlserver://" + dbURL + ";databaseName=OutdoorParadise;user=" + dbLogin + ";password=" + dbPassword);
        }
        return conn;
    }
}
